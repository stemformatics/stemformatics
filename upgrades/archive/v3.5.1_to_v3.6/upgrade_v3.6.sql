-- v3.6 upgrades beforehand
-- mainly for groups which is task 686 - have been saving it as task 558
drop table public.shared_resources;

CREATE TABLE stemformatics.groups (
    gid SERIAL,
    group_name TEXT NOT NULL,
    PRIMARY KEY (gid)
);

CREATE TABLE stemformatics.group_users (
    gid INTEGER NOT NULL,
    uid INTEGER NOT NULL,
    role TEXT NOT NULL,
    PRIMARY KEY (gid,uid)
);


CREATE INDEX uid_gid_index on stemformatics.group_users (uid,gid);

CREATE TABLE stemformatics.group_configs (
    gid INTEGER NOT NULL,
    config_type TEXT NOT NULL,
    config_name TEXT NOT NULL,
    config_value TEXT NOT NULL,
    db_id INTEGER NULL,
    PRIMARY KEY (gid,config_type,config_name)
);


-- need to share a job, gene set, gene expression graph and dataset overrides
-- stemformatics.override_private_datasets, add object_type and object_id and set current ones to "User" and uid
-- stemformatics.gene_sets add object_type and object_id and set current ones to "User" and uid
-- stemformatics.shared_resources add object_type and object_id and set current ones to "User" and uid
alter table stemformatics.override_private_datasets drop constraint override_private_datasets_uid_fkey;
alter table stemformatics.override_private_datasets add column object_type TEXT NOT NULL default '';
alter table stemformatics.override_private_datasets add column object_id TEXT NOT NULL default '';
update stemformatics.override_private_datasets set object_type = 'User',object_id = uid;
alter table stemformatics.override_private_datasets drop constraint  override_private_datasets_pkey;
alter table stemformatics.override_private_datasets add constraint override_private_datasets_pkey primary key (ds_id,object_type,object_id) ;
alter table stemformatics.override_private_datasets alter uid set default 0;


-- Task #465 - yu gene check
ALTER TABLE datasets ADD COLUMN show_yugene boolean NOT NULL default True;
update datasets set show_yugene = false where id = 6084;
update datasets set show_yugene = false where id = 6089;
update datasets set show_yugene = false where id = 6081;
update datasets set show_yugene = false where id = 6083;
update datasets set show_yugene = false where id = 6111;



-- v3.6 upgrades to do after deployment


insert into stemformatics.groups values (1,'Project Grandiose');
insert into stemformatics.group_configs values(1,'UCSC Links','Merged Samples','http://stemformatics.qern.qcif.edu.au/ucsc_tracks/ProjectGrandiose/RNASeq_merged.txt',46);

update stemformatics.users set is_admin = true where uid in (1,2,3);


-- convert all users who have access to 5037 to be in group 1 for Project Grandiose
insert into stemformatics.group_users (gid,uid,role) select 1,cast(object_id as int),'User' from stemformatics.override_private_datasets where object_type = 'User' and ds_id = 5037;

-- task 717 removing detection Thresholds
update dataset_metadata set ds_value = '' where ds_id in (6075,6076) and ds_name = 'detectionThreshold';







-- This is for task 569 line graph
insert into biosamples_metadata values(72,'6033111110_A','Day','Day 0',5037);
insert into biosamples_metadata values(72,'6033111110_B','Day','Day 02',5037);
insert into biosamples_metadata values(72,'6033111110_C','Day','Day 05',5037);
insert into biosamples_metadata values(72,'6033111110_D','Day','Day 08',5037);
insert into biosamples_metadata values(72,'6033111110_E','Day','Day 11',5037);
insert into biosamples_metadata values(72,'6033111110_F','Day','Day 16',5037);
insert into biosamples_metadata values(72,'6033111110_G','Day','Day 18',5037);
insert into biosamples_metadata values(72,'6033111110_H','Day','Pluripotent Cells',5037);
insert into biosamples_metadata values(72,'6033111113_A','Day','Pluripotent Cells',5037);
insert into biosamples_metadata values(72,'6033111113_B','Day','Pluripotent Cells',5037);
insert into biosamples_metadata values(72,'6033111113_C','Day','Day 16',5037);
insert into biosamples_metadata values(72,'6033111113_D','Day','Day 21',5037);
insert into biosamples_metadata values(72,'6033111113_E','Day','Day 16',5037);
insert into biosamples_metadata values(72,'6033111113_F','Day','Day 16',5037);
insert into biosamples_metadata values(72,'6033111113_G','Day','Day 21',5037);
insert into biosamples_metadata values(72,'6033111113_H','Day','Day 21',5037);



insert into biosamples_metadata values(72,'6033111110_A','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_B','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_C','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_D','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_E','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_F','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_G','LineGraphGroup','High',5037);
insert into biosamples_metadata values(72,'6033111110_H','LineGraphGroup','Pluripotent Cells',5037);
insert into biosamples_metadata values(72,'6033111113_A','LineGraphGroup','Pluripotent Cells',5037);
insert into biosamples_metadata values(72,'6033111113_B','LineGraphGroup','Pluripotent Cells',5037);
insert into biosamples_metadata values(72,'6033111113_C','LineGraphGroup','Low',5037);
insert into biosamples_metadata values(72,'6033111113_D','LineGraphGroup','Low',5037);
insert into biosamples_metadata values(72,'6033111113_E','LineGraphGroup','Low',5037);
insert into biosamples_metadata values(72,'6033111113_F','LineGraphGroup','Low',5037);
insert into biosamples_metadata values(72,'6033111113_G','LineGraphGroup','Low',5037);
insert into biosamples_metadata values(72,'6033111113_H','LineGraphGroup','Low',5037);
insert into dataset_metadata values(5037,'lineGraphOrdering','{"Day 0":1,"Day 02":2,"Day 05":3,"Day 08":4,"Day 11":5,"Day 16":6,"Day 18":7,"Day 21":8,"Pluripotent Cells":9}');

-- -------------------------------------------------------------------------------------------------------------

insert into biosamples_metadata values(101,'D0','Day','Day 0',6073);
insert into biosamples_metadata values(101,'D11','Day','Day 11',6073);
insert into biosamples_metadata values(101,'D16APG2r2','Day','Day 16',6073);
insert into biosamples_metadata values(101,'D16BPG2r2','Day','Day 16',6073);
insert into biosamples_metadata values(101,'D16PG2r1','Day','Day 16',6073);
insert into biosamples_metadata values(101,'D16','Day','Day 16',6073);
insert into biosamples_metadata values(101,'D18','Day','Day 18',6073); 
insert into biosamples_metadata values(101,'D21PG2r1','Day','Day 21',6073);
insert into biosamples_metadata values(101,'D21PG2r2DoxMinus','Day','Day 21',6073);
insert into biosamples_metadata values(101,'D21PG2r2DoxPlus','Day','Day 21',6073);
insert into biosamples_metadata values(101,'D2','Day','Day 02',6073);
insert into biosamples_metadata values(101,'D5','Day','Day 05',6073);
insert into biosamples_metadata values(101,'D8','Day','Day 08',6073);
insert into biosamples_metadata values(101,'IPS1o','Day','Pluripotent Cells',6073);
insert into biosamples_metadata values(101,'IPS2o','Day','Pluripotent Cells',6073);
insert into biosamples_metadata values(101,'rtTA','Day','Pluripotent Cells',6073);

insert into biosamples_metadata values(101,'D0','LineGraphGroup','High',6073);
insert into biosamples_metadata values(101,'D11','LineGraphGroup','High',6073);
insert into biosamples_metadata values(101,'D16APG2r2','LineGraphGroup','Low',6073);
insert into biosamples_metadata values(101,'D16BPG2r2','LineGraphGroup','Low',6073);
insert into biosamples_metadata values(101,'D16PG2r1','LineGraphGroup','Low',6073);
insert into biosamples_metadata values(101,'D16','LineGraphGroup','High',6073);
insert into biosamples_metadata values(101,'D18','LineGraphGroup','High',6073); 
insert into biosamples_metadata values(101,'D21PG2r1','LineGraphGroup','Low',6073);
insert into biosamples_metadata values(101,'D21PG2r2DoxMinus','LineGraphGroup','Low',6073);
insert into biosamples_metadata values(101,'D21PG2r2DoxPlus','LineGraphGroup','Low',6073);
insert into biosamples_metadata values(101,'D2','LineGraphGroup','High',6073);
insert into biosamples_metadata values(101,'D5','LineGraphGroup','High',6073);
insert into biosamples_metadata values(101,'D8','LineGraphGroup','High',6073);
insert into biosamples_metadata values(101,'IPS1o','LineGraphGroup','Pluripotent Cells',6073);
insert into biosamples_metadata values(101,'IPS2o','LineGraphGroup','Pluripotent Cells',6073);
insert into biosamples_metadata values(101,'rtTA','LineGraphGroup','Pluripotent Cells',6073);

insert into dataset_metadata values(6073,'lineGraphOrdering','{"Day 0":1,"Day 02":2,"Day 05":3,"Day 08":4,"Day 11":5,"Day 16":6,"Day 18":7,"Day 21":8,"Pluripotent Cells":9}');

