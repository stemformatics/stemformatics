CREATE TABLE stemformatics.feature_mappings (
    db_id INT REFERENCES annotation_databases (an_database_id) NOT NULL,
    chip_type INT REFERENCES assay_platforms (chip_type) NOT NULL,
    from_type TEXT NOT NULL,
    from_id TEXT NOT NULL,
    to_type TEXT NOT NULL,
    to_id TEXT NOT NULL,
    PRIMARY KEY (db_id, chip_type,from_type,from_id,to_type,to_id)
);

CREATE INDEX to_index on stemformatics.feature_mappings (db_id,chip_type,to_type,to_id);
    
