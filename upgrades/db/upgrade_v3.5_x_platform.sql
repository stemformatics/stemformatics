-- Task #451 

-- Have to pip install redis for the python virtual environment

-- Task #465 x_platform

--  probe mappings for over 100
-- speed up by putting chip_type on the datasets table
ALTER TABLE datasets ADD COLUMN chip_type integer NOT NULL default 0;
-- populate
-- update datasets set chip_type = (select chip_type from stemformatics.probe_expressions_avg_replicates where ds_id = id limit 1) where id = 2000;
update datasets set chip_type = (select chip_type from stemformatics.probe_expressions_avg_replicates where ds_id = id limit 1);


------ Upgrade ds -id 4000 manually

Update biosamples_metadata set 
md_value = 'luminal_GSM511133' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586022_C';
 

Update biosamples_metadata set 
md_value = 'MaSC_GSM511131' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586023_F'; 
 
Update biosamples_metadata set 
md_value = 'MaSC_GSM511130' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586023_E'; 
 

Update biosamples_metadata set 
md_value = 'MaSC_GSM511129' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586023_C'; 

Update biosamples_metadata set 
md_value = 'luminal_GSM511128' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586023_B'; 
  

Update biosamples_metadata set 
md_value = 'luminal_GSM511125' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586024_E'; 

Update biosamples_metadata set 
md_value = 'luminal_GSM511124' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586024_D'; 


Update biosamples_metadata set 
md_value = 'MaSC_GSM511123' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586024_C'; 

Update biosamples_metadata set 
md_value = 'luminal_GSM511122' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586024_A'; 


Update biosamples_metadata set 
md_value = 'MaSC_GSM511135' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586022_F'; 

Update biosamples_metadata set 
md_value = 'luminal_GSM511134' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586022_E'; 

Update biosamples_metadata set 
md_value = 'MaSC_GSM511132' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586022_B'; 


Update biosamples_metadata set 
md_value = 'luminal_GSM511127' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586023_A'; 

Update biosamples_metadata set 
md_value = 'MaSC_GSM511126' 
where ds_id = 4000 and md_name = 'Replicate Group ID' and 
chip_id = '4936586001_C';  



-- GENERAL v3.5 updates go here (non x-platform)

-- Task #452 - Default visibility for datasets is now:  published=True, private=True
-- Coupled with default UIDs entered into stemformatics.override_private_datasets
-- when we load new datasets, this means we don't need to worry about manually 
-- adding devs to the white-list for new datasets (but we'll still need to add the
-- owner of the dataset to the override)

alter table datasets alter private set default true;
alter table datasets alter published set default true;


-- Task #500 adding multi_datasets_view_46 and multi_datasets_view_56 in users_metadata
CREATE TABLE stemformatics.users_metadata (
    uid INTEGER NOT NULL,
    md_name TEXT NOT NULL,
    md_value TEXT NOT NULL,
    PRIMARY KEY (uid,md_name)
);
