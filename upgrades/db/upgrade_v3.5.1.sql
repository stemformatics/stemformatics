-- Already DONE!
-- task #614 

-- ALTER TABLE assay_platforms ADD COLUMN min_y_axis real NOT NULL default 0;
-- ALTER TABLE assay_platforms ADD COLUMN y_axis_label text NOT NULL default '';
-- ALTER TABLE assay_platforms ADD COLUMN y_axis_label_description text NOT NULL default '';


--All microarray platforms should have the y- axis as Log-2 Expression. 
-- update assay_platforms set y_axis_label = 'Log2 Expression' where  platform_type = 'microarray';
--The RNAseq should be RKPM (Log2)
-- update assay_platforms set y_axis_label = 'RKPM (Log2)' where  platform_type = 'HTS';
--The CAGE should be TPM (log2)
-- update assay_platforms set y_axis_label = 'TPM (Log2)' where  platform_type = 'CAGE';
--The qPCR should be 1/CT
-- update assay_platforms set y_axis_label = '1/CT' where  platform_type = 'Fluidigm Single Cell qPCR';
--The protein should be Log2 ratio but I need to check this
-- update assay_platforms set y_axis_label = 'Log2 Ratio' where  chip_type = 105;


--The miRNA and other seq platforms (chip and methyl) I need to check
--update assay_platforms set y_axis_label = 'RPM (Log2)' where  platform_type = 'miRNA';


-- ALTER TABLE datasets ADD COLUMN min_y_axis text NOT NULL default '';

-- task #613 - feature mappings mapping id
-- ALTER TABLE stemformatics.feature_mappings RENAME COLUMN chip_type TO mapping_id;
-- ALTER TABLE assay_platforms ADD COLUMN mapping_id integer NOT NULL default 0;
-- update assay_platforms set mapping_id = chip_type;
-- drop table feature_mappings ;
-- alter table stemformatics.feature_mappings drop constraint feature_mappings_chip_type_fkey;
-- drop index stemformatics.to_index;
-- CREATE INDEX to_index on stemformatics.feature_mappings (db_id,mapping_id,to_type,to_id);


-- Task #
--insert into dataset_metadata (ds_id,ds_name,ds_value) select id,'genePatternAnalysisAccess','Disable' from datasets as d left join assay_platforms as ap on ap.chip_type = d.chip_type where platform_type != 'microarray';


-- delete from stemformatics.users_metadata where md_name = 'MULTIVIEW';
