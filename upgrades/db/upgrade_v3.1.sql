-- Story # 169 - improve speed of validate using probes
CREATE INDEX quick_to_index on stemformatics.feature_mappings (db_id,to_type,to_id,from_type);


-- Task #344 - issue with GN view results
CREATE INDEX to_id_index on stemformatics.feature_mappings (db_id,to_id);
CREATE INDEX from_id_index on stemformatics.feature_mappings (db_id,from_id);


-- Update to remove secondary Cell Type Short or Cell Type
-- UPDATE dataset_metadata SET ds_value = 'Sample Type' where ds_name = 'limitSortBy' and ds_id in (5018,5016,5015,5025,5027,5028);
