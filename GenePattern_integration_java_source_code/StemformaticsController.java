
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Properties;

import org.genepattern.client.GPClient;
import org.genepattern.webservice.JobResult;
import org.genepattern.webservice.Parameter;
import org.genepattern.webservice.WebServiceException;

public class StemformaticsController {
    public static void main(String[] args)
        throws Exception {
	   
	   
        String StemformaticsJobID = args[0];


        // read these variables from pylons configuration file
        String StemformaticsIni = args[1];

        File ConfigIni=new File(StemformaticsIni);
        System.out.println("Getting command line variables");

        boolean existsConfigIni = ConfigIni.exists();

        if (!existsConfigIni){
              System.exit(0);
        } 
        // Get configuration items
        Properties p = new Properties();
        p.load(new FileInputStream(StemformaticsIni));
      
        String base_dir_gpqueue = p.getProperty("GPQueue");
        String base_dir_stemformaticsqueue = p.getProperty("StemformaticsQueue");
        String base_url = p.getProperty("StemformaticsInternalBaseUrl");
        String base_gene_pattern_url = p.getProperty("GenePatternServer");
        String gene_pattern_username = p.getProperty("GenePatternUsername");
        String gene_pattern_password = p.getProperty("GenePatternPassword");
        String base_dir_dataset_gct_files = p.getProperty("DatasetGCTFiles");
        String base_dir_dataset_cls_files = p.getProperty("DatasetCLSFiles");
        String base_gene_pattern_job_results_url = p.getProperty("GenePatternJobResults");

        System.out.println("Getting configuration variables");
	  
        // Get Job.ini files
        String JobIniFile= base_dir_stemformaticsqueue+StemformaticsJobID+"/job.ini";
        Properties job_ini = new Properties();
        job_ini.load(new FileInputStream(JobIniFile));

        Integer AnalysisID = Integer.parseInt(job_ini.getProperty("analysis"));
        File file=new File(base_dir_gpqueue);

        boolean exists = file.exists();
        if (!exists){
              
            boolean success = file.mkdir();
            if (success){
                System.out.println("The base directory was successfully created");
            }
        }

        // No longer using Comparative Marker Selection analysis 1
	  
        // now have to handle different urls
        // analysis ids of 0,1 and 2 use gene_pattern_download
        int [] use_gene_pattern_download = new int[]{ 0,2,7 };

        int indexOfGPDownload = Arrays.binarySearch(use_gene_pattern_download,AnalysisID);

        if (indexOfGPDownload >= 0) { 
            String urlString = base_url+ "api/gene_pattern_download/"+StemformaticsJobID;
            String filename = base_dir_gpqueue+ "download_file"+StemformaticsJobID+".log";

            wget(urlString,filename);
        }

        GPClient gpClient=new GPClient(base_gene_pattern_url, gene_pattern_username,gene_pattern_password);

        if (AnalysisID == 0){
            createHierarchicalClusterImage(base_dir_stemformaticsqueue,StemformaticsJobID,gpClient,base_dir_gpqueue,base_url,job_ini);
        } // end analysis 0

        if (AnalysisID ==2 || AnalysisID ==7){
            createGeneNeighbourhoodResult(base_dir_stemformaticsqueue,StemformaticsJobID,gpClient,base_dir_gpqueue,base_url,job_ini);
        }

        if (AnalysisID ==4){
            createGeneSetAnnotationCPickleFile(base_dir_stemformaticsqueue,StemformaticsJobID,base_url,job_ini);
        }
      
   }
   
   
   public static void wget(String urlString, String filename){

	  
	  BufferedInputStream in = null;
	  FileOutputStream fout = null;
	  try
	  {

  			  in = new BufferedInputStream(new URL(urlString).openStream());

	          fout = new FileOutputStream(filename);
	
	          byte data[] = new byte[1024];
	          int count;
	          while ((count = in.read(data, 0, 1024)) != -1)
	          {
	                  fout.write(data, 0, count);
	          }
	  }
	  
	  catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	  finally
	  {
		  try {
	          if (in != null)
	                  in.close();
	          if (fout != null)
	                  fout.close();
		  }
		  catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  
	  }	  

 	   
    }   
		   
		  
   
   
    public static void createHierarchicalClusterImage(
		   String base_dir_stemformaticsqueue, 
		   String StemformaticsJobID,
		   GPClient gpClient,
		   String base_dir_gpqueue,
		   String base_url,
		   Properties job_ini
		   ){
	   
	   String inputDataset= base_dir_stemformaticsqueue+StemformaticsJobID+"/job.gct";
		  
		  File test_file = new File(inputDataset);
		  System.out.println(inputDataset);

	      if (test_file.isFile()) {
	    	  
	    	  JobResult preprocess = null;
                  Integer column_distance_measure = Integer.parseInt(job_ini.getProperty("column_distance_measure"));
                  Integer row_distance_measure = Integer.parseInt(job_ini.getProperty("row_distance_measure"));
                System.out.println(column_distance_measure);
                System.out.println(row_distance_measure);
			try {
				preprocess = gpClient.runAnalysis("HierarchicalClustering", 
				          new Parameter[] {
					new Parameter("input.filename", inputDataset),
					new Parameter("column.distance.measure", column_distance_measure),
					new Parameter("row.distance.measure", row_distance_measure),
					new Parameter("clustering.method", "a"),
					new Parameter("output.base.name", "hc")
					
					});
			} catch (WebServiceException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

	    	  System.out.println("This has finished the HC");
	    	  String downloadDirName=String.valueOf(preprocess.getJobNumber());
				
	    	  String base_folder = base_dir_gpqueue;
	    	  
	    	  File[] outputFiles = null;
			try {
				outputFiles = preprocess.downloadFiles(base_folder + downloadDirName);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    	  
	    	  
                // check for stderr.txt on the first name
                String first_file_name = outputFiles[0].getName();
                if (first_file_name.equals("stderr.txt")){ 
                    System.out.println(first_file_name);
                    System.out.println("Failed job. Need to alert Stemformatics");

                    String filenameFinalise = base_folder + "download_file"+StemformaticsJobID+".log";
                    String urlFinaliseString = base_url + "api/update_job/"+StemformaticsJobID +"?status=2&reference_type=GenePattern&reference_id="+preprocess.getJobNumber();
                    wget(urlFinaliseString,filenameFinalise);
                    return;
                } 

                String cdt_filename = ""; 
                String gtr_filename = ""; 
                String atr_filename = ""; 
                for (int i=0;i < outputFiles.length;i++) {
                    String thisFile = base_folder + downloadDirName + "/" + outputFiles[i].getName();
                    if (thisFile.indexOf("cdt") != -1 ) { 
                        cdt_filename = thisFile; 
                    }
                    if (thisFile.indexOf("gtr") != -1 ) { 
                        gtr_filename = thisFile; 
                    }
                    if (thisFile.indexOf("atr") != -1 ) { 
                        atr_filename = thisFile; 
                    }

                }
                                  
	    	  
                Integer row_and_column_size = Integer.parseInt(job_ini.getProperty("row_and_column_size"));
	    	  
                JobResult viewHC = null;
		try {
                    System.out.println(column_distance_measure);
                    System.out.println(row_distance_measure);
                    String colour_scheme = "row normalized";  
                    if (column_distance_measure == 7 || row_distance_measure ==7){ 
                        colour_scheme = "global";  
                    }

                    if (column_distance_measure == 2 || row_distance_measure ==2){
                        colour_scheme = "row normalized";  
                    } 

                    viewHC = gpClient.runAnalysis("HierarchicalClusteringImage", 
                    new Parameter[] {
					new Parameter("cdt", cdt_filename),
					new Parameter("gtr", gtr_filename),
					new Parameter("atr", atr_filename),  
					new Parameter("output", "hc"),
					new Parameter("output.format", "png"),
					new Parameter("column.size", row_and_column_size),
					new Parameter("row.size", row_and_column_size),
					new Parameter("color.scheme", colour_scheme),
					new Parameter("show.grid", "yes"),
					new Parameter("show.row.descriptions", "no"),
					new Parameter("show.row.names", "yes")
					
					});
			} catch (WebServiceException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    	  
	    	  
	    	  	String downloadDirName2=String.valueOf(base_folder + viewHC.getJobNumber());
				
                        // download result files
                        File[] outputFiles2 = null;
                        try {
                                outputFiles2 = viewHC.downloadFiles(downloadDirName2);
                        } catch (IOException e1) {
                                // TODO Auto-generated catch block
                                e1.printStackTrace();
                        }


                        // check for stderr.txt on the first name
                        first_file_name = outputFiles2[0].getName();
                        if (first_file_name.equals("stderr.txt")){ 
                            System.out.println(first_file_name);
                            System.out.println("Failed job. Need to alert Stemformatics");

                            String filenameFinalise = base_folder + "download_file"+StemformaticsJobID+".log";
                            String urlFinaliseString = base_url + "api/update_job/"+StemformaticsJobID +"?status=2&reference_type=GenePattern&reference_id="+viewHC.getJobNumber();
                            wget(urlFinaliseString,filenameFinalise);
                            return;
                        } 
                            
                        File inputFile = new File(cdt_filename);
                        File outputFile = new File(downloadDirName2 + "/hc.cdt");

                        FileReader inFile = null;
                            try {
                                    inFile = new FileReader(inputFile);
                            } catch (FileNotFoundException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                            }
                        FileWriter outFile = null;
                            try {
                                    outFile = new FileWriter(outputFile);
                            } catch (IOException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                            }
                        int c;

                        try {
                                    while ((c = inFile.read()) != -1)
                                      outFile.write(c);
                            } catch (IOException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                            } try {
                                    inFile.close();
                            } catch (IOException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                            }
                        try {
                                    outFile.close();
                            } catch (IOException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                            }
                            
                            
                            System.out.println(outputFiles2[0].getName());
                            
                            // Runtime.getRuntime().exec("eog /tmp/GPQueue/"+ Integer.toString(viewHC.getJobNumber()) +"/" + outputFiles2[0].getName());
                            
                            String urlFinaliseString = base_url + "api/update_job/"+StemformaticsJobID +"?status=1&reference_type=GenePattern&reference_id="+viewHC.getJobNumber();
                            String filenameFinalise = base_folder + "download_file"+StemformaticsJobID+".log";
                            
                            System.out.println(urlFinaliseString);
                            
                            wget(urlFinaliseString,filenameFinalise);
                            
	      } else {
	    	  System.out.println("This is not a valid file. Please try again");
	    	  
	      }	   
   
   }
   
   
  
   
    public static void createGeneNeighbourhoodResult(
        String base_dir_stemformaticsqueue, 
        String StemformaticsJobID,
        GPClient gpClient,
        String base_dir_gpqueue,
        String base_url,
        Properties job_ini
        ) throws FileNotFoundException{

        String gene = job_ini.getProperty("gene");
        String probe = job_ini.getProperty("probe");
		  
		  
        // need to get the ensemble gene id and find out the gene symbol
        String inputDatasetGCT= base_dir_stemformaticsqueue+StemformaticsJobID+"/job.gct";
      
		  
        File test_file_gct = new File(inputDatasetGCT);
      
        System.out.println(inputDatasetGCT);
	      
        if (test_file_gct.isFile()) {

            System.out.println("There are two files");

            FileInputStream fstream = new FileInputStream(inputDatasetGCT);
            // Get the object of DataInputStream
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String secondLine = "";
            //Read File Line By Line
            try {
                        br.readLine();
                        secondLine = br.readLine();
                        in.close();
                
            } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
            }

            String[] statistics = secondLine.split("\\t");

            String number_of_probes = statistics[0];

            System.out.println(number_of_probes);



            JobResult result = null;
            try {
                result = gpClient.runAnalysis("urn:lsid:broad.mit.edu:cancer.software.genepattern.module.analysis:00007:4", 
                new Parameter[]{
                    new Parameter("data.filename", inputDatasetGCT), 
                    new Parameter("gene.accession", probe), // name in the gct file
                    new Parameter("num.neighbors", number_of_probes), 
                    new Parameter("marker.list.file", "<data.filename_basename>.markerlist"), 
                    new Parameter("marker.dataset.file", "<data.filename_basename>.markerdata"), 
                    new Parameter("distance.metric", "3"), 
                    new Parameter("filter.data", "no"), 
                    new Parameter("min.threshold", "10"), 
                    new Parameter("max.threshold", "16000"), 
                    new Parameter("min.fold.diff", "5"), 
                    new Parameter("min.abs.diff", "50")});
            } catch (WebServiceException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

            System.out.println("This has finished the Gene Neighbours");
            String base_folder = base_dir_gpqueue;
            String downloadDirNameCMS=String.valueOf(base_folder + "/" + result.getJobNumber());
              
            // download result files
            File[] outputFiles = null;
            try {
                    outputFiles = result.downloadFiles(downloadDirNameCMS);
            } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
            }


            // check for stderr.txt on the first name
            String first_file_name = outputFiles[0].getName();
            if (first_file_name.equals("stderr.txt")){ 
                System.out.println(first_file_name);
                System.out.println("Failed job. Need to alert Stemformatics");

                String filenameFinalise = base_folder + "download_file"+StemformaticsJobID+".log";
                String urlFinaliseString = base_url + "api/update_job/"+StemformaticsJobID +"?status=2&reference_type=GenePattern&reference_id="+result.getJobNumber();
                wget(urlFinaliseString,filenameFinalise);
                return;
            } 

          
            String urlFinaliseString = base_url + "api/update_job/"+StemformaticsJobID +"?status=1&reference_type=GenePattern&reference_id="+result.getJobNumber();
            String filenameFinalise = base_dir_gpqueue+"download_file"+StemformaticsJobID+".log";
        
            System.out.println(urlFinaliseString);
          
            wget(urlFinaliseString,filenameFinalise);
          
      } else {
          System.out.println("No files found");
          
      }

   
	   
   }

   // createGeneSetAnnotationCPickleFile(base_dir_stemformaticsqueue,StemformaticsJobID,base_url,job_ini);
   /*
    * All we need to do is call api/gene_set_annotation_job/88 for job #88. This will create a cPickle file in the right folder
    * 
    * 
    */
   public static void createGeneSetAnnotationCPickleFile(
		   String base_dir_stemformaticsqueue, 
		   String StemformaticsJobID,
		   String base_url,
		   Properties job_ini
		   ){

	   
	    String urlString = base_url+ "api/gene_set_annotation_job/"+StemformaticsJobID;
		String filename = base_dir_stemformaticsqueue+ "/" + StemformaticsJobID + "/download_file.log";
	  
		wget(urlString,filename);
	   
		String urlFinaliseString = base_url + "api/update_job/"+StemformaticsJobID +"?status=1";
		String filenameFinalise = base_dir_stemformaticsqueue+ "/" + StemformaticsJobID + "/finalise_download_file.log";
		
	
  	  	System.out.println(urlFinaliseString);
  	  
  	  	wget(urlFinaliseString,filenameFinalise);
  	
	   
	   
   }

   
}



