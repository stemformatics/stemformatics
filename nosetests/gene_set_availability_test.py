from guide.model.stemformatics import *
from guide.model.graphs import *
import psycopg2
import psycopg2.extras


class tempData(object):
    pass

temp = tempData() 

def test_datasets():

    gene_set_id = 2627
    uid = 0
    result = Stemformatics_Gene_Set.check_gene_set_availability(gene_set_id,uid)
    assert result == True

    gene_set_id = 2679
    uid = 0
    result = Stemformatics_Gene_Set.check_gene_set_availability(gene_set_id,uid)
    assert result == False

    gene_set_id = 2679
    uid = 3
    result = Stemformatics_Gene_Set.check_gene_set_availability(gene_set_id,uid)
    assert result == True
