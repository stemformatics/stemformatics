from pylons import request, response, session, url, tmpl_context as c
from sqlalchemy import or_, and_, desc
from sqlalchemy.exceptions import *
import json
import logging
log = logging.getLogger(__name__)

# Live querying
from guide.model.stemformatics import *
from pylons import config
import redis
import psycopg2
import psycopg2.extras
import re


def test_1():


    search='"bone marrow"'

    uid= 3


    get_samples = True
    all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search,get_samples,uid,all_sample_metadata)

    datasets = temp_result['datasets']
    samples = temp_result['all_samples_by_ds_id']
    other_samples = temp_result['all_samples']


    assert 1000 in datasets
    assert 6290 in datasets
    assert 5041 in datasets

    assert len(datasets) >= 118


def test_2():
    search="chemokines"
    uid= 3


    get_samples = True
    all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search,get_samples,uid,all_sample_metadata)

    datasets = temp_result['datasets']
    samples = temp_result['all_samples_by_ds_id']
    other_samples = temp_result['all_samples']


    assert 6371 in datasets
    assert 6806 in datasets

    assert len(datasets) >= 13





def test_3():
    search="ipsc esc"
    uid= 3


    get_samples = True
    all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search,get_samples,uid,all_sample_metadata)

    datasets = temp_result['datasets']
    samples = temp_result['all_samples_by_ds_id']
    other_samples = temp_result['all_samples']


    assert len(datasets) >= 11


def test_4():


    search='"bone marrow" ipsc esc'

    uid= 3


    get_samples = True
    all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search,get_samples,uid,all_sample_metadata)

    datasets = temp_result['datasets']
    samples = temp_result['all_samples_by_ds_id']
    other_samples = temp_result['all_samples']


    assert 1000 in datasets
    assert 6290 in datasets
    assert 5041 in datasets

    assert len(datasets) >= 138

def test_5():


    search='synovial "stem cell aging"'

    uid= 3


    get_samples = True
    all_sample_metadata = Stemformatics_Expression.setup_all_sample_metadata()
    temp_result = Stemformatics_Dataset.find_datasets_and_samples(search,get_samples,uid,all_sample_metadata)

    datasets = temp_result['datasets']
    samples = temp_result['all_samples_by_ds_id']
    other_samples = temp_result['all_samples']


    assert 6658 in datasets
    assert 6288 in datasets
    assert 6544 in datasets

    assert len(datasets) >= 7


