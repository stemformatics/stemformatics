from guide.model.stemformatics import *
from guide.model.graphs import *

species_dict = Stemformatics_Gene.get_species(db)

class tempData(object):
    pass


temp = tempData() 

def test_view_for_mirna():
    temp.ds_id = 6128
    temp.db_id = 46
    temp.ensembl_id = ''
    temp.ref_type = 'miRNA'
    temp.ref_id = 'MI0000154'
    temp.sortBy = 'Sample Type'
    temp.list_of_samples_to_remove = [] 
    temp.line_graph_available = False

    temp.expected_chip_type = 103
    temp.expected_data_levels = 2
    temp.expected_handle ='Clancy_2013_under_review_PRIVATE' 
    temp.expected_x_axis_label = 'D5H'
    temp.expected_title_id = ' for miRNA mir-127'
    temp.expected_xaxis_label_type_bar_and_box = 'Probe'
    temp.expected_disease_x_axis_label = 'normal' 
    temp.expected_probe_list = ['mi0000154', 'mimat0000139', 'mimat0004530', 'mmu-mir-127-3p', 'mmu-mir-127-5p']

    setup_graphs(temp)



def setup_graphs(temp):
    this_graph_data = Graph_Data(db,temp.ds_id,temp.ref_type,temp.ref_id,temp.db_id,temp.list_of_samples_to_remove,species_dict)
    assert this_graph_data.handle == temp.expected_handle 
    assert this_graph_data.ref_type == temp.ref_type
    assert this_graph_data.ref_id == temp.ref_id
    assert this_graph_data.db_id == temp.db_id
    assert this_graph_data.chip_type == temp.expected_chip_type


    this_graph = Scatterplot_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert temp.expected_x_axis_label in this_graph.xaxis_labels
    assert this_graph.data_levels == temp.expected_data_levels
    print this_graph.graph_data.probe_list
    assert this_graph.graph_data.probe_list == temp.expected_probe_list

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert temp.expected_x_axis_label in this_view.graph.xaxis_labels
    assert this_view.graph.data_levels == temp.expected_data_levels


    this_graph = Box_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert this_graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_graph.data_levels == temp.expected_data_levels

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert this_view.graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_view.graph.data_levels == temp.expected_data_levels


    this_graph = Bar_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert this_graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_graph.data_levels == temp.expected_data_levels

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert this_view.graph.xaxis_labels[0]['type'] == temp.expected_xaxis_label_type_bar_and_box
    assert this_view.graph.data_levels == temp.expected_data_levels

    print this_view.graph.graph_data.probe_list
    assert this_view.graph.graph_data.probe_list == temp.expected_probe_list

    # disease state
    temp.sortBy = 'Disease State'
    this_graph = Scatterplot_Graph(this_graph_data,temp.sortBy) 
    assert isinstance(this_graph.plot_data,dict)
    assert temp.expected_disease_x_axis_label in this_graph.xaxis_labels
    assert this_graph.data_levels == temp.expected_data_levels

    this_view = Preview(this_graph,temp.line_graph_available)
    assert this_view.title_id == temp.expected_title_id
    assert this_view.ensembl_id == temp.ensembl_id
    assert this_view.line_graph_available == False
    assert temp.expected_disease_x_axis_label in this_view.graph.xaxis_labels
    assert this_view.graph.data_levels == temp.expected_data_levels


    # line graph - leave for now.


def test_feature_search_autocomplete():

    feature_search_term = 'mir-127'
    species = None
    feature_type = 'miRNA'

    data = Stemformatics_Gene.autocomplete_feature_search_items(feature_search_term,species,feature_type)
    assert data == '[{"description": "Mus musculus mir-127 stem-loop", "symbol": "mir-127", "feature_id": "MI0000154", "feature_type": "miRNA", "species": "Mus musculus", "aliases": "MI0000154,MIMAT0004530,mmu-mir-127-5p,MIMAT0000139,mmu-mir-127-3p"}, {"description": "Homo sapiens mir-127 stem-loop", "symbol": "mir-127", "feature_id": "MI0000472", "feature_type": "miRNA", "species": "Homo sapiens", "aliases": "MI0000472,MIMAT0004604,hsa-mir-127-5p,MIMAT0000446,hsa-mir-127-3p"}, {"description": "Homo sapiens mir-1271 stem-loop", "symbol": "mir-1271", "feature_id": "MI0003814", "feature_type": "miRNA", "species": "Homo sapiens", "aliases": "MI0003814,MIMAT0005796,hsa-mir-1271-5p,MIMAT0022712,hsa-mir-1271-3p"}, {"description": "Homo sapiens mir-1272 stem-loop", "symbol": "mir-1272", "feature_id": "MI0006408", "feature_type": "miRNA", "species": "Homo sapiens", "aliases": "MI0006408,MIMAT0005925,hsa-mir-1272"}, {"description": "Homo sapiens mir-1275 stem-loop", "symbol": "mir-1275", "feature_id": "MI0006415", "feature_type": "miRNA", "species": "Homo sapiens", "aliases": "MI0006415,MIMAT0005929,hsa-mir-1275"}, {"description": "Homo sapiens mir-1276 stem-loop", "symbol": "mir-1276", "feature_id": "MI0006416", "feature_type": "miRNA", "species": "Homo sapiens", "aliases": "MI0006416,MIMAT0005930,hsa-mir-1276"}, {"description": "Homo sapiens mir-1277 stem-loop", "symbol": "mir-1277", "feature_id": "MI0006419", "feature_type": "miRNA", "species": "Homo sapiens", "aliases": "MI0006419,MIMAT0022724,hsa-mir-1277-5p,MIMAT0005933,hsa-mir-1277-3p"}, {"description": "Homo sapiens mir-1278 stem-loop", "symbol": "mir-1278", "feature_id": "MI0006425", "feature_type": "miRNA", "species": "Homo sapiens", "aliases": "MI0006425,MIMAT0005936,hsa-mir-1278"}, {"description": "Homo sapiens mir-1279 stem-loop", "symbol": "mir-1279", "feature_id": "MI0006426", "feature_type": "miRNA", "species": "Homo sapiens", "aliases": "MI0006426,MIMAT0005937,hsa-mir-1279"}]'


    feature_search_term = 'mir-127'
    species = 'Mus musculus'
    feature_type = 'miRNA'

    data = Stemformatics_Gene.autocomplete_feature_search_items(feature_search_term,species,feature_type)
    assert data == '[{"description": "Mus musculus mir-127 stem-loop", "symbol": "mir-127", "feature_id": "MI0000154", "feature_type": "miRNA", "species": "Mus musculus", "aliases": "MI0000154,MIMAT0004530,mmu-mir-127-5p,MIMAT0000139,mmu-mir-127-3p"}]'

    feature_search_term = 'mir-147'
    species = None
    feature_type = None

    data = Stemformatics_Gene.autocomplete_feature_search_items(feature_search_term,species,feature_type)
    assert data == '[{"description": "Homo sapiens mir-147a stem-loop", "symbol": "mir-147a", "feature_id": "MI0000262", "feature_type": "miRNA", "species": "Homo sapiens", "aliases": "MI0000262,MIMAT0000251,hsa-mir-147a"}, {"description": "Mus musculus mir-147 stem-loop", "symbol": "mir-147", "feature_id": "MI0005482", "feature_type": "miRNA", "species": "Mus musculus", "aliases": "MI0005482,MIMAT0017269,mmu-mir-147-5p,MIMAT0004857,mmu-mir-147-3p"}, {"description": "Homo sapiens mir-147b stem-loop", "symbol": "mir-147b", "feature_id": "MI0005544", "feature_type": "miRNA", "species": "Homo sapiens", "aliases": "MI0005544,MIMAT0004928,hsa-mir-147b"}, {"description": "Homo sapiens mir-1470 stem-loop", "symbol": "mir-1470", "feature_id": "MI0007075", "feature_type": "miRNA", "species": "Homo sapiens", "aliases": "MI0007075,MIMAT0007348,hsa-mir-1470"}, {"description": "Homo sapiens mir-1471 stem-loop", "symbol": "mir-1471", "feature_id": "MI0007076", "feature_type": "miRNA", "species": "Homo sapiens", "aliases": "MI0007076,MIMAT0007349,hsa-mir-1471"}]'





def test_feature_search_autocomplete_errors():
    feature_search_term = 'mir-127'
    species = 'Mus m171usculus'
    feature_type = 'miRNA'

    data = Stemformatics_Gene.autocomplete_feature_search_items(feature_search_term,species,feature_type)
    assert data == '[]'


    feature_search_term = 'stem loop'
    species = 'Mus m171usculus'
    feature_type = 'miRNA'

    data = Stemformatics_Gene.autocomplete_feature_search_items(feature_search_term,species,feature_type)
    assert data == '[]'



def test_new_feature_annotation_find_feature_search_items():
    feature_search_term = 'mir-127'
    db_id = None
    feature_type = 'all'
    use_json = None
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data[0]['feature_id'] == 'MI0000154'
    assert data[0]['symbol'] == 'mir-127'
    assert data[0]['aliases'] == 'MI0000154,MIMAT0004530,mmu-mir-127-5p,MIMAT0000139,mmu-mir-127-3p'
    assert data[0]['feature_type'] == 'miRNA'
    assert data[0]['species'] == 'Mus musculus'
    assert data[0]['description'] == 'Mus musculus mir-127 stem-loop'
    assert data[8]['symbol'] == 'mir-1279'
    assert len(data) ==9
    assert len(data) ==9

    feature_type = 'miRNA'
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data[0]['feature_id'] == 'MI0000154'
    assert data[0]['symbol'] == 'mir-127'
    assert data[8]['symbol'] == 'mir-1279'
    assert len(data) ==9

    feature_search_term = 'mmu-mir-127'
    feature_type = 'miRNA'
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data[0]['feature_id'] == 'MI0000154'
    assert data[0]['symbol'] == 'mir-127'
    assert len(data) ==1

    feature_search_term = 'mmu-mir-127'
    feature_type = 'all'
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data[0]['feature_id'] == 'MI0000154'
    assert data[0]['symbol'] == 'mir-127'
    assert len(data) ==1

    feature_search_term = '127'
    feature_type = 'all'
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data[0]['feature_id'] == 'MI0000154'
    assert data[0]['symbol'] == 'mir-127'
    assert data[21]['symbol'] == 'mir-5195'
    assert len(data) ==22


    feature_search_term = '147'
    feature_type = 'all'
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data[0]['feature_id'] == 'MI0000147'
    assert data[0]['symbol'] == 'mir-99b'
    assert data[10]['symbol'] == 'mir-5588'
    assert len(data) ==11


def test_new_find_feature_search_errors():
    feature_search_term = ''
    db_id = None
    feature_type = 'all'
    use_json = None
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data == None

    feature_search_term = None
    db_id = None
    feature_type = 'all'
    use_json = None
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data == None

    feature_search_term = 'stem loop'
    db_id = None
    feature_type = 'miRNA'
    use_json = None
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data == None



    feature_search_term = ''
    db_id = None
    feature_type = 'miRNA'
    use_json = None
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data == None

    feature_search_term = None
    db_id = None
    feature_type = 'miRNA'
    use_json = None
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data == None

    feature_search_term = 'stat1'
    db_id = None
    feature_type = 'miRNA'
    use_json = None
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data == None

    feature_search_term = 'stat1'
    db_id = None
    feature_type = 'all'
    use_json = None
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data == None

    feature_search_term = 'stat1'
    db_id = None
    feature_type = None
    use_json = None
    data = Stemformatics_Gene.find_feature_search_items(feature_search_term,db_id,feature_type,use_json)
    assert data == None


