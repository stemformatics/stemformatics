from pylons import request, response, session, url, tmpl_context as c,config
import json
from guide.model.stemformatics import *



def test_1():

    result = Stemformatics_Dataset.get_thomson_reuters_feed()
    assert result[5018]['Contact Email']  == 'joewu@stanford.edu'
    assert result[2000]['Year']  == '2010'
    assert result[4000]['GEO Accession']  == 'GSE20402'
    assert result[4000]['Title']  == 'Mouse mammary epithelial cell subpopulations from pregnant and virgin mice'
    assert len(result) > 100

    file_text = Stemformatics_Dataset.create_thomson_reuters_xml_file(result)
    print file_text
    assert file_text == True


