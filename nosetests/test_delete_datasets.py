import unittest
import logging
log = logging.getLogger(__name__)

import string
import json, redis
import os.path
from pylons import request, response, session, tmpl_context as c, url, app_globals as g,config

from guide.model.stemformatics import *

def test_delete_disk_files():
    ds_id = 6101
    result = Stemformatics_Dataset.delete_dataset_files_from_disk(ds_id)

    cumulative_file_path = config['x_platform_base_dir'] + 'dataset'+str(ds_id) +'.cumulative.txt'
    assert os.path.exists(cumulative_file_path) == False

    cls_file_path = config['DatasetCLSFiles'] + str(ds_id) + '*'
    assert os.path.exists(cls_file_path) == False

    probe_file_path = config['DatasetProbeFiles'] + str(ds_id) + '.probes'
    assert os.path.exists(probe_file_path) == False

    gct_file_path = config['DatasetGCTFiles'] + 'dataset'+str(ds_id) + '.cls'
    assert os.path.exists(gct_file_path) == False

    sd_file_path = config['DatasetStandardDeviationFiles'] + 'probe_expression_avg_replicates_' +str(ds_id) + '.txt'
    assert os.path.exists(sd_file_path) == False

    assert result == "Removed CLS FIles, GCT Files, Standard Deviation Files, Probe Files, Cumulative Files"

def test_delete_dataset_redis():
    ds_id = 2000
    result = Stemformatics_Dataset.delete_dataset_redis(ds_id)
    r_server = redis.Redis(unix_socket_path=config['redis_server'])
    delimiter = config['redis_delimiter']
    assert r_server.exists('cumulative_labels|'+str(ds_id)) == False
    assert r_server.exists('gct_labels|'+str(ds_id)) == False
    assert result == "Redis deleted Successfully"

def test_get_list_of_deleted_datasets():
    result = Stemformatics_Dataset.get_list_of_deleted_datasets()
    assert result is not None

def test_delete_dataset_from_database():
    result = Stemformatics_Dataset.delete_dataset_from_database(2000)
    assert result == "Deleted data from biosamples_metadata, biosamples, dataset_metadata, datasets, override_private_datasets, stats_datasetsummary "
