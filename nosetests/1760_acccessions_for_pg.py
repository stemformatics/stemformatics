"""

    nosetests --with-pylons=config.ini guide/model/stemformatics/test_stemformatics_dataset.py


"""

from guide.model.stemformatics import *

from pylons import config
import psycopg2
import psycopg2.extras

sra_accession_id = 'SRATEST1'
ena_accession_id = 'ENATEST1'
pxd_accession_id = 'PXDTEST1'
sra_ds_name ='SRA Accession'
ena_ds_name ='ENA Accession'
pxd_ds_name ='PXD Accession'
ds_name_list=[sra_ds_name , ena_ds_name ,pxd_ds_name]


conn_string = config['psycopg2_conn_string']

def _delete_all_test_data(ds_id):

    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("delete from  dataset_metadata where ds_id = %s and ds_name = ANY(%s);",(ds_id,ds_name_list,))
    # retrieve the records from the database
    conn.commit()
    cursor.close()
    conn.close()



def _create_test_data(ds_id,ds_name,accession_id):

    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("delete from  dataset_metadata where ds_id = %s and ds_name = %s ;",(ds_id,ds_name,))
    # retrieve the records from the database
    conn.commit()
    cursor.close()
    conn.close()

    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("insert into  dataset_metadata values(%s,%s,%s) ;",(ds_id,ds_name,accession_id,))
    # retrieve the records from the database
    conn.commit()
    cursor.close()
    conn.close()

def test_accessions():
    
    resultList= {}
    ds_id = 2000
    ds_mt_result = {}
    uid = 3

    _delete_all_test_data(ds_id)

    dataset_result = Stemformatics_Dataset.getDatasetDetails(db,ds_id,uid)
    #new_dataset_result = _new_getDatasetDetails(db,ds_id,uid)

    # assert dataset_result == new_dataset_result


    assert dataset_result[ds_id]['ae_accession_id'] == 'E-TABM-724'
    assert dataset_result[ds_id]['pxd_accession_id'] == 'N/A'
    assert dataset_result[ds_id]['sra_accession_id'] == 'N/A'
    assert dataset_result[ds_id]['ena_accession_id'] == 'N/A'

    _create_test_data(ds_id,sra_ds_name,sra_accession_id)


    dataset_result = Stemformatics_Dataset.getDatasetDetails(db,ds_id,uid)
    assert dataset_result[ds_id]['ae_accession_id'] == 'E-TABM-724'
    assert dataset_result[ds_id]['pxd_accession_id'] == 'N/A'
    assert dataset_result[ds_id]['sra_accession_id'] == sra_accession_id
    assert dataset_result[ds_id]['ena_accession_id'] == 'N/A'

    _create_test_data(ds_id,pxd_ds_name,pxd_accession_id)

    dataset_result = Stemformatics_Dataset.getDatasetDetails(db,ds_id,uid)
    assert dataset_result[ds_id]['ae_accession_id'] == 'E-TABM-724'
    assert dataset_result[ds_id]['pxd_accession_id'] == pxd_accession_id
    assert dataset_result[ds_id]['sra_accession_id'] == sra_accession_id
    assert dataset_result[ds_id]['ena_accession_id'] == 'N/A'

    _create_test_data(ds_id,ena_ds_name,ena_accession_id)

    dataset_result = Stemformatics_Dataset.getDatasetDetails(db,ds_id,uid)
    assert dataset_result[ds_id]['ae_accession_id'] == 'E-TABM-724'
    assert dataset_result[ds_id]['pxd_accession_id'] == pxd_accession_id
    assert dataset_result[ds_id]['sra_accession_id'] == sra_accession_id
    assert dataset_result[ds_id]['ena_accession_id'] == ena_accession_id



"""
This is not in use
"""
def _new_getDatasetDetails(db,ds_id,uid):

    resultList= {}
    ds_mt_result = {}

    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("select * from dataset_metadata where ds_id = %s ;",(ds_id,))
    # retrieve the records from the database
    ds_metadata_result = cursor.fetchall()
    cursor.close()
    conn.close()


    for r in ds_metadata_result:
        ds_mt_result[r['ds_name']] = r['ds_value']

    resultList[ds_id] = Stemformatics_Dataset._encodeData(ds_mt_result)            

<<<<<<< HEAD


    db.schema = 'stemformatics'
    ds_stats = db.stats_datasetsummary
    
    ds_mt_result['breakDown'] = {}
    
    for breakDown in ds_stats.filter(ds_stats.ds_id==ds_id).order_by(ds_stats.md_name).all():
         ds_mt_result['breakDown'][breakDown.md_name + ': ' + breakDown.md_value] = breakDown.count 
    
    # have to get values for topDifferentiallyExpressedGenes
    try:
        top_diff_exp_genes_temp = ds_mt_result["topDifferentiallyExpressedGenes"]
    except:
        top_diff_exp_genes_temp = ""
    
    top_diff_exp_genes = {}
    
    if top_diff_exp_genes_temp == "NULL" or top_diff_exp_genes_temp == "" :
        top_diff_exp_genes = {}
        
#            if ds_mt_result['Organism'] == 'Homo sapiens':
#                top_diff_exp_genes_temp = config['human_default_genes_of_interest']
#            else:
#                top_diff_exp_genes_temp = config['mouse_default_genes_of_interest']
#            
    try:
    
        for symbol in top_diff_exp_genes_temp.split(','):
            db.schema="public"
            findGene = db.genome_annotations.filter(db.genome_annotations.gene_id==symbol).first()
            top_diff_exp_genes[findGene.associated_gene_name] = { 'ensemblID':symbol, 'db_id':findGene.db_id}
    except:
        top_diff_exp_genes = {}
    ds_mt_result["topDifferentiallyExpressedGenes"] = top_diff_exp_genes            

   

    
    resultList[dataSet.id] = Stemformatics_Dataset._encodeData(ds_mt_result)
    new_handle = Stemformatics_Dataset.add_extra_to_handle(db,dataSet.handle,dataSet.private,dataSet.show_limited)
    resultList[dataSet.id]['handle'] = new_handle
    resultList[dataSet.id]['limitSortBy'] = ds_mt_result["limitSortBy"]    
    resultList[dataSet.id]['dataset_status'] = dataset_status
    resultList[dataSet.id]['number_of_samples'] = dataSet.number_of_samples
    resultList[dataSet.id]['chip_type'] = dataSet.chip_type



    return resultList

