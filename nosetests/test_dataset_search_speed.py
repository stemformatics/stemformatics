"""

    nosetests --with-pylons=config.ini guide/model/stemformatics/test_stemformatics_dataset.py


"""

import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *

from guide.lib.state import *
from pylons import config
useSqlSoup = True

log.debug('Testing in guide/model/stemformatics/test_stemformatics_dataset.py')

'''def test_getHandle_1000():
    returnData = Stemformatics_Dataset.getHandle(db,1000)
    print returnData
    assert returnData == 'Blood cells (human)'

def test_getHandle_2000():
    returnData = Stemformatics_Dataset.getHandle(db,2000)
    print returnData
    assert returnData == 'hONS cells' '''
    
def test_getHandle_3000():
    returnData = Stemformatics_Dataset.getHandle(db,3000)
    print returnData
    assert returnData == 'Lim_2009_19648928'


def test_dataset_search_speed():
    search = 'cell'
    uid = 0
    datasets = Stemformatics_Dataset.dataset_search(db, uid,search, True)
    print datasets
    assert datasets == 'Lim_2009_19648928'


