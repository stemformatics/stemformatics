import logging
log = logging.getLogger(__name__)


import sqlalchemy as SA
from sqlalchemy import or_, and_, desc

import re
import string
import json

from guide.model.stemformatics import *
from guide.model.tfv import *

from guide.lib.state import *

useSqlSoup = True

log.debug('Testing in guide/model/stemformatics/test_stemformatics.py')

def test_result_data_slpi_4000_uncollapsed():
    useCollapsedData = True
    db_id = 46
    returnData = Stemformatics_Expression.result_data(db,'ENSMUSG00000017002',4000,useCollapsedData,db_id)
    print returnData[0]
    assert returnData[0]['sortByLabelling'] == 'Cy3'


def test_result_data_stat1_2000_uncollapsed():
    useCollapsedData = True
    db_id = 56
    returnData = Stemformatics_Expression.result_data(db,'ENSG00000115415',2000,useCollapsedData,db_id)
    print returnData[0]
    assert returnData[0]['expression_value'] == 10.419270589931299


def test_result_data_pias4_4000_collapsed():
    useCollapsedData = False
    db_id = 46
    returnData = Stemformatics_Expression.result_data(db,'ENSMUSG00000004934',4000,useCollapsedData,db_id)
    print returnData[0]
    assert returnData[0]['sortByLabelling'] == 'Cy3'
    assert returnData[0]['expression_value'] == 8.9412917127812399

def test_result_data_stat1_2000_collapsed():
    useCollapsedData = False
    db_id = 56
    returnData = Stemformatics_Expression.result_data(db,'ENSG00000115415',2000,useCollapsedData,db_id)
    print returnData[0]
    assert returnData[0]['expression_value'] == 10.155979093561999


def test_result_data_error():
    useCollapsedData = False
    db_id = 46
    returnData = Stemformatics_Expression.result_data(db,'ENSG00000115415',2000,useCollapsedData,db_id)
    print returnData
    assert returnData == {}
    
def test_result_data_error_2():
    useCollapsedData = False
    db_id = 56
    returnData = Stemformatics_Expression.result_data(db,'ENSMUSG00000004934',4000,useCollapsedData,db_id)
    print returnData
    assert returnData == {}
