#!/bin/bash

py_number_of_lines=`find Portal/guide -type f -name "*.py" -exec wc -l {} \; | grep -Po [0-9]* | paste -sd+ - | bc`
mako_number_of_lines=`find Portal/guide -type f -name "*.mako" -exec wc -l {} \; | grep -Po [0-9]* | paste -sd+ - | bc`
js_number_of_lines=`find Portal/guide -type f -name "*.js" -exec wc -l {} \; | grep -Po [0-9]* | paste -sd+ - | bc`
css_number_of_lines=`find Portal/guide -type f -name "*.css" -exec wc -l {} \; | grep -Po [0-9]* | paste -sd+ - | bc`

total=$(( py_number_of_lines + mako_number_of_lines + js_number_of_lines + css_number_of_lines))

echo "py $py_number_of_lines"
echo "mako $mako_number_of_lines"
echo "js $js_number_of_lines"
echo "css $css_number_of_lines"
echo "======================"
echo $total
