.. GuIDE Developers

GuIDE Developers
================

`Jarny Choi <mailto:jchoi@wehi.edu.au>`_:
    Original site design and implementation

`Carolyn de Graaf <mailto:degraaf@wehi.edu.au>`_:
    Scientist / Technical Advisor

`Tobias Sargeant <mailto:sargeant@wehi.edu.au>`_:
    Scientist / Technical Advisor
    
`Nick Seidenman <mailto:seidenman@wehi.edu.au>`_:
    SQL and ORM models
    
