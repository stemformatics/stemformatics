#!/bin/bash

DS_IDS=$@
for ds_id in $DS_IDS
do
    echo "Entering $ds_id"
    echo "============================"
    cd /var/www/pylons-data/prod/PCAFiles/$ds_id
    PCAfolder_list=()
    cmd=''
    for i in $(ls -d */);
    do
        PCAfolder_list+=(${i%%/})

     done
    echo "Folders found - ${PCAfolder_list[@]}"
    echo "=============================================="

    #now iterate over all ds_ids
    for i in "${PCAfolder_list[@]}"
        do
             echo "started ds_id --> $i"
             cmd+="INSERT INTO dataset_metadata (ds_id,ds_name,ds_value) VALUES (${ds_id} , 'ShowPCALinksOnDatasetSummaryPage' , '{\"name\":\"${i//_/ }\", \"url\":\"/datasets/pca?ds_id=${ds_id}&pca_type=${i}\"}' );"
             echo "finished ds_id --> $i"
             echo "======================"
        done
    echo ${cmd}
    psql -U portaladmin portal_prod -c " ${cmd}"
    echo "All done"
done
