#!/bin/bash
## ==========================================================================
## portal-admin: Script to manage deployment of Stem Cell Portal instances.
## This could *almost* work for managing the deployment of generic Pylons 
## applications, but there are a few things in here that are Portal specific.
##
## For usage information, run script with no args.
##
## Author: o.korn
##
## TODO:
##	* Database settings per pylons instance (not global)
##
## ==========================================================================

## Set some variables we'll use later on.
cwd=`pwd`
datetime=`date +"%Y%m%d-%H%M"`
date=`date +"%Y%m%d"`
lockfile="/tmp/portal-admin.lock"
SCRIPTS_DIR=`dirname $0`
PROFILE_DIR="$SCRIPTS_DIR/profiles"

## Script args
userProfile="$2"
userInstance="$3"
userCmd="$4"


function IsValidTarget {
  target="$1"
  for c in $VALID_CONFIGS; do
    if [ "$c" = "$target" ]; then
      echo "true"
      return 0
    fi
  done 
  echo "false"
}

function Exit {
  exitCode="$1"
  if [ -f $lockfile ]; then
    rm $lockfile
  fi
  if [ -f "/tmp/portal-admin.pgpass" ]; then
    rm "/tmp/portal-admin.pgpass"
  fi
  exit $exitCode
}

function Usage {
  echo; echo "Usage: $0 -c <profile_settings> <portal_instance> <command>"
  echo    "Where:"
  echo    "     'profile_settings' is the file containing server profile settings."
  if [ -z $VALID_CONFIGS ]; then
    echo  "     'portal_instance' is a valid instance target (see VALID_CONFIGS in profile settings file)"
  else
    echo -n "     'portal_instance' := "
    for c in $VALID_CONFIGS; do
      echo -n "$c|"
    done
    echo    "  (matching sub-dir in $PYLONS_BASE)"
  fi
  echo    "     'command' := start|stop|restart|status|deploy"; echo
  Exit
}

function LoadDataCacheOptsFromPylonsConfig {
  ## Get file cache directory path
  #targetline=`grep -P "^filedir\s*\=" "$PYLONS_CFG_FILE" 2> /dev/null`
  #if [ $? -ne 0 ]; then
  #  echo "Error: Failed to determine db file cache directory from pylons .ini file."
  #  echo "  Check for existence of config item 'filedir'"; echo
  #  return 1
  #fi
  #dirpath=`echo "$targetline" | sed -r -e 's/^.*\=\s*//g' | tr -d [:cntrl:]`
  #export DB_FILE_CACHE="$dirpath"
  ### DEBUG ###
  #echo "DEBUG: DB_FILE_CACHE=[$DB_FILE_CACHE]"
  ### END ###

  ## Get gene mapping cache file name
  targetline=`grep -P "^gene_mapping_raw_file_base_name\s*\=" "$PYLONS_CFG_FILE" 2> /dev/null`
  if [ $? -ne 0 ]; then
    echo "Error: Failed to determine gene mapping cache file name from pylons .ini file."
    echo "  Check for existence of config item 'gene_mapping_raw_file_base_name'"; echo
    return 1
  fi
  genefilename=`echo "$targetline" | sed -r -e 's/^.*\=\s*//g' | tr -d [:cntrl:]`
  export DB_GENE_FILE="$genefilename"
  ### DEBUG ###
  #echo "DEBUG: DB_GENE_FILE=[$DB_GENE_FILE]"
  ### END ###

  ## Get probe mapping cache file name
  targetline=`grep -P "^feature_mapping_raw_file_base_name\s*\=" "$PYLONS_CFG_FILE" 2> /dev/null`
  if [ $? -ne 0 ]; then
    echo "Error: Failed to determine probe mapping cache file name from pylons .ini file."
    echo "  Check for existence of config item 'feature_mapping_raw_file_base_name'"; echo
    return 1
  fi
  probefilename=`echo "$targetline" | sed -r -e 's/^.*\=\s*//g' | tr -d [:cntrl:]`
  export DB_PROBE_FILE="$probefilename"
  ### DEBUG ###
  #echo "DEBUG: DB_PROBE_FILE=[$DB_PROBE_FILE]"
  #sleep 3
  ### END ###
}

function UpdateProbeGeneFileCacheFromDatabase {
  psql_options="-h $DB_HOST -U $DB_USER"
  if [ ! -z "$DB_PORT" ]; then
    psql_options="$psql_options -p $DB_PORT"
  fi
  psql_options="$psql_options $DB_NAME"

  ### DEBUG ###
  #echo "DEBUG: psql_options=[$psql_options]"; echo
  #sleep 3
  ### END ###

  ## Regenerate probe mappings cache
  echo "Refreshing '$DB_PROBE_FILE' from database .."
  psql $psql_options -c "COPY (SELECT * FROM stemformatics.feature_mappings) TO STDOUT;" > "$DB_PROBE_FILE.new"
  if [ $? -ne 0 ]; then
    echo "Error: Failed to retrieve probe mapping data from DB, probe map cache not built."
    return 1
  fi
  if [ -f "$DB_PROBE_FILE" ]; then
    mv "$DB_PROBE_FILE" "$DB_PROBE_FILE.bak"
  fi
  mv "$DB_PROBE_FILE.new" "$DB_PROBE_FILE"

  ## Regenerate gene mapping cache
  echo "Refreshing '$DB_GENE_FILE' from database.."
  psql $psql_options -c "COPY (SELECT db_id,gene_id,associated_gene_name,associated_gene_synonym,entrezgene_id,refseq_dna_id,description FROM genome_annotations) TO STDOUT;" > "$DB_GENE_FILE.new"
  if [ $? -ne 0 ]; then
    echo "Error: Failed to retrieve gene annotation data from DB, gene map cache not built."
    return 1
  fi
  if [ -f "$DB_GENE_FILE" ]; then
    mv "$DB_GENE_FILE" "$DB_GENE_FILE.bak"
  fi
  mv "$DB_GENE_FILE.new" "$DB_GENE_FILE"
}

function RefreshDiskFileCache {
  LoadDataCacheOptsFromPylonsConfig
  if [ $? -ne 0 ]; then
    return 1
  fi
  ## Create temporary "PGPASSFILE" for 'psql' automatic password usage.
  ## See: http://www.postgresql.org/docs/current/static/libpq-pgpass.html
  export PGPASSFILE="/tmp/portal-admin.pgpass"
  echo "$DB_HOST:$DB_PORT:$DB_NAME:$DB_USER:$DB_PASS" > "$PGPASSFILE"
  ## If we don't set mode 600, 'psql' will ignore our password file.
  chmod 600 $PGPASSFILE

  UpdateProbeGeneFileCacheFromDatabase
  if [ $? -ne 0 ]; then
    return 1
  fi
}

function LoadDBSettingsFromPylonsConfig {
  targetline=`grep "guide.model.stemformatics.db.url" "$PYLONS_CFG_FILE" 2> /dev/null`
  if [ $? -ne 0 ]; then
    echo "Error: Failed to determine database DSN from pylons .ini file."
    echo "  Check for existence of config item 'guide.model.stemformatics.db.url'"; echo
    return 1
  fi

  dsnpart=`echo "$targetline" | grep -P -v "^#" | sed -r -e 's/^.*\=\s*//g' -e 's|^.*\:\/\/||g'`
  hostdbpart=`echo "$dsnpart" | sed -r -e 's/^.*\@//g'`
  escapedhostdbpart=`echo "$hostdbpart" | sed -r -e 's|\:|\\\:|g' -e 's|/|\\/|g'`
  userpart=`echo "$dsnpart" | sed -r -e "s|\@$escapedhostdbpart||"`
    
  ### DEBUG ###
  #echo "DEBUG: Got User part [$userpart]"
  #echo "DEBUG: Got Host/DB part [$hostdbpart]"
  #sleep 3
  ### END DEBUG ###

  echo "$userpart" | grep ":" > /dev/null 2>&1
  ## Assume format user:pass if ":" character found
  if [ $? -eq 0 ]; then
    u=`echo $userpart | sed -r -e s'/\:.*$//g'`
    p=`echo $userpart | sed -r -e s'/^.*\://g'`
  else
    u=$userpart
    p=""
  fi

  d=`echo "$hostdbpart" | sed -r -e 's|^.*\/||g'`
  h=`echo "$hostdbpart" | sed -r -e 's|\/.*$||g'`
  echo "$h" | grep ":" > /dev/null 2>&1
  ## Assume port number given if ":" character found
  if [ $? -eq 0 ]; then
    pt=`echo "$h" | sed -r -e 's/^.*\://g'`
    h=`echo "$h" | sed -r -e 's/\:.*$//g'`
  fi

  ## Export DB settings
  export DB_HOST="$h"
  export DB_USER="$u"
  export DB_PASS="$p"
  export DB_NAME="$d"
  if [ ! -z "$pt" ]; then
    export DB_PORT="$pt"
  fi
 
  ### DEBUG ###
  #echo "DEBUG: Database configuration loaded:"
  #echo "DB_HOST=[$DB_HOST]"
  #echo "DB_USER=[$DB_USER]"
  #echo "DB_PASS=[$DB_PASS]"
  #echo "DB_PORT=[$DB_PORT]"
  #echo "DB_NAME=[$DB_NAME]"
  #echo
  #sleep 3
  ### END DEBUG ###
}

function PreStartupTasks {
  pylons_cfg="`eval echo \\$CONFIG_$1`"
  pylons_cfg_path="$PYLONS_BASE/$1/$pylons_cfg"
  export PYLONS_CFG_FILE="$pylons_cfg_path"

  echo "Running Stemformatics Pylons pre-startup tasks.."; sleep 2
  if [ ! -f "$pylons_cfg_path" ]; then
    echo "ERROR: Bad config file path "$pylons_cfg_path"!"
    return 1
  fi
  #echo "DEBUG: Got pylons config file '$pylons_cfg_path' .."

  ## Task 1. Rebuild disk files from database data, as necessary
  ## Determine connection settings from 'stemformatics' db DSN entry
  LoadDBSettingsFromPylonsConfig "$pylons_cfg_path"
  if [ $? -ne 0 ]; then
    echo "ERROR: Failed to load DB settings from pylons configuration file."
    return 1
  fi
  CheckDatabaseCredentials
  if [ $? -ne 0 ]; then
    return 1
  fi
  RefreshDiskFileCache
  if [ $? -ne 0 ]; then
    echo "ERROR: Failed to regenerate disk file cache/s from DB."
    return 1
  fi
}

function PostStartupTasks {
  pylons_cfg="`eval echo \\$CONFIG_$1`"
  pylons_cfg_path="$PYLONS_BASE/$1/$pylons_cfg"
  export PYLONS_CFG_FILE="$pylons_cfg_path"

  echo "Running Stemformatics Pylons post-startup tasks.."; sleep 2
  if [ ! -f "$pylons_cfg_path" ]; then
    echo "ERROR: Bad config file path "$pylons_cfg_path"!"
    return 1
  fi

  ## Task 2. Call "setup_all_datasets" URI to regenerate "all_sample_metadata.cpickle disk file"
  which wget > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "ERROR: Failed to find 'wget' tool (required for sample metadata cache refresh)"
    return 1
  fi
  ## Get the pylons server URI
  pylonshost=127.0.0.1
  configport=`grep -P "^port" "$PYLONS_CFG_FILE" 2> /dev/null`
  if [ $? -ne 0 ]; then
    echo "Error: Failed to determine Pylons server port from pylons .ini file."
    echo "  Check for existence of config item 'port'"; echo
    return 1
  fi
  pylonsport=`echo "$configport" | sed -r -e 's/^port\s+\=\s+//' | tr -d [:cntrl:]`
  echo "Executing duty tasks.."; sleep 2
  ## do samples metadata cpickle file update
  ## Do we need this any more??
  wget --no-proxy -q --load-cookies /tmp/.s4mcookies.txt --keep-session-cookies http://$pylonshost:$pylonsport/expressions/setup_all_sample_metadata -O /tmp/.wgetout
  echo "  done"; echo
}

function StartPylons {
  target="$1"
  echo "Starting pylons/paster instance '$target'.. "
  if [ ! -w "$PYLONS_BASE/log" ]; then 
    echo "Error: Cannot write to pylons log directory ($PYLONS_BASE/log) as current user ($USER)!"
    Exit 1
  fi

  PreStartupTasks "$target"
  if [ $? -ne 0 ]; then
    echo "[ FAIL ]"
    echo "Error: Pre-startup tasks failure. Aborting pylons launch."
    exit 1
  fi

  (cd "$PYLONS_BASE/$target" && $VIRTUALENV/bin/paster serve --log-file "$PYLONS_BASE/log/$target.log" --daemon "`eval echo \\$CONFIG_$target`")
  ## Wait until timeout period for Pylons to start
  timeout=30
  counter=0
  is_running="false"
  while [ $counter -lt $timeout ]; do
    if [ `IsRunning "$target"` = "false" ]; then
      echo -n "+"
      sleep 1
      counter=`expr $counter + 1`
    else
      is_running="true"; echo
      break
    fi
  done

  ## We suppose everything is okay if the correct port is up and listening for connections
  if [ "$is_running" = "false" ]; then
    echo "[ FAIL ]"
    echo "Error: Paster/Pylons instance '$target' not running! Last 50 lines of log file:"; echo
    tail -50 "$PYLONS_BASE/log/$target.log"; echo
    exit 1
  else
    sleep 2
    PostStartupTasks "$target"
    if [ $? -ne 0 ]; then
      echo "WARNING: Post-startup tasks failure; manual system integrity check required."
    fi
    ### END ###
    echo "[ OK ]"
  fi
}

function StopPylons {
  target="$1"
  echo "Stopping pylons/paster instance '$target'.."
  ## Kill paster instances whose full command line contains our target config file name
  pkill -u $USER -f `eval echo \\$CONFIG_$target`
  if [ "$?" -ne 0 ]; then
    echo "[ FAIL ]"
    echo "Error: Pylons/paster instance was not found. Please check status manually."; echo
    Exit 1 
  else
    echo "[ OK ]"
  fi
}

function RestartPylons {
  target="$1"
  StopPylons "$target"
  StartPylons "$target"
}

function StatusPylons {
  target="$1"
  echo -n "Pylons/paster instance '$target' is "
  if [ `IsRunning "$target"` = "true" ]; then
    echo "running on port `eval echo \\$PORT_$target`."; echo
  else
    echo "NOT running on port `eval echo \\$PORT_$target`."; echo
  fi
}

function IsRunning {
  target="$1"
  port=`eval echo \\$PORT_$target`
  netstat -an | grep -E [\d\.]\*\:$port | grep -v grep > /dev/null 2>&1
  if [ "$?" -ne 0 ]; then
    echo "false"
  else
    echo "true"
  fi
}

## Get parsed / var replaced version of input file (taking multiple %VAR%=<value> args
## for token replacement).
##
ReplaceTokens () {
  template="$1"
  shift

  sed_cmd="cat $template | sed -r "
  sed_opts=""

  while [ ! -z "$1" ];
  do
    kv="$1"
    key=`echo "$kv" | cut -d'=' -f 1`
    val=`echo "$kv" | cut -d'=' -f 2`
    sed_opts="$sed_opts -e \"s|$key|$val|g\""
    shift
  done

  ## DEBUG
  #echo "[eval $sed_cmd $sed_opts]"

  eval "$sed_cmd $sed_opts"
}


function ReplaceTokensInFileFromINI {
  replacetarget="$1"
  keyvalini="$2"

  replace_spec=""
  ## iterate over key/val pairs to build replacement spec
  while read line
  do
    echo "$line" | grep -P "^#" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      continue
    fi
    ## if "a = b" then trim spaces around the equal sign
    line=`echo "$line" | sed -r -e 's|\s+\=\s+|\=|'`
    ## extract key and value
    srckey=`echo "$line" | cut -d'=' -f 1`
    srcval=`echo "$line" | cut -d'=' -f 2-`
    ## NOTE: srcval must not contain spaces
    replace_spec="$replace_spec %$srckey%=$srcval"
  done < "$keyvalini"

  ReplaceTokens "$replacetarget" $replace_spec > "${replacetarget}.new"
  mv "${replacetarget}.new" "$replacetarget"
}

function AddPasswordsToINI {
  replacetarget="$1"
  pwd_repo="$REPO_SRC/../s4m_passwords"
  if [ ! -d "$pwd_repo" ]; then
    echo "Error: Could not find passwords database in location '$REPO_SRC/../s4m_passwords'"
    Exit 1
  fi
  if [ ! -f "$pwd_repo/passwords.ini" ]; then
    echo "Error: Could not find master password .ini file in location '$REPO_SRC/../s4m_passwords/passwords.ini'"
    Exit 1
  fi
  ReplaceTokensInFileFromINI "$replacetarget" "$pwd_repo/passwords.ini"
}

function DeployPylons {
  target="$1"
  echo "(Re)deploying pylons/paster instance '$target' from repository source.."

  if [ `IsRunning "$target"` = "true" ]; then
    StopPylons "$target"
  fi

  if [ -d "$PYLONS_BASE/$target" ]; then
    if [ ! -w $PYLONS_BASE ]; then
      echo "Error: Cannot write to pylons home ($PYLONS_BASE) as user $USER!"
      echo "Please check directory ownership and write permissions and try again."
      Exit 1
    fi
    ## Clean out the old directory
    rm -rf "$PYLONS_BASE/$target"
  fi

  ## Check that our portal source exists
  if [ ! -d $REPO_SRC/Portal ]; then
    echo "Error: Source pylons instance directory '$REPO_SRC/Portal' not found!"
    echo  "Aborting."
    Exit 1
  fi
  ## Copy the source tree to the deployment location
  cp -R $REPO_SRC/Portal "$PYLONS_BASE/$target"

  ## Copy target config to pylons top level directory, if it exists
  if [ -f "$PYLONS_BASE/$target/conf/`eval echo \\$CONFIG_$target`" ]; then
    pylonsini="$PYLONS_BASE/$target/`eval echo \\$CONFIG_$target`"
    cp "$PYLONS_BASE/$target/conf/`eval echo \\$CONFIG_$target`" "$pylonsini"
    ## inject passwords and other secure tokens into .ini file
    AddPasswordsToINI "$pylonsini" 
  else
    echo "Error: Could not locate target pylons \"ini\" file: $PYLONS_BASE/$target/conf/`eval echo \\$CONFIG_$target`"
    echo "Aborting."
    Exit 1
  fi

  ## Create log directory if it doesn't already exist
  mkdir -p $PYLONS_BASE/log

  ## Note: Not applying any "patches" over repository source.
  ## If needed, this should be done here.
  #echo "Applying mods to source.."
  
  ## Rebuild Portal library (egg)
  echo "Rebuilding Portal library.."
  cd "$PYLONS_BASE/$target"
  $VPYTHON setup.py build > /dev/null
  sleep 1

  ## Make all group-writeable to make editing easier on deployed instance
  chmod -R g+w "$PYLONS_BASE/$target"

  StartPylons "$target"
}

function CheckDatabaseCredentials {
  which psql > /dev/null 2>&1
  if [ "$?" -ne 0 ]; then
    echo "Error: Failed to find 'psql' (postgresql) client!"; echo
    #Exit 1
    return 1
  fi
  if [ -z "$DB_HOST" ]; then
    echo "Error: DB_HOST not defined in profile settings!"; echo
    #Exit 1
    return 1
  fi
  if [ -z "$DB_PORT" ]; then
    echo "Error: DB_PORT not defined in profile settings!"; echo
    #Exit 1
    return 1
  fi
  if [ -z "$DB_USER" ]; then
    echo "Error: DB_USER not defined in profile settings!"; echo
    #Exit 1
    return 1
  fi
  if [ -z "$DB_PASS" ]; then
    echo "Error: DB_PASS not defined in profile settings!"; echo
    #Exit 1
    return 1
  fi
  #if [ -z "$DB_SCRIPTS" ]; then
  #  echo "Error: No database scripts defined in DB_SCRIPTS setting!"; echo
  #  Exit 1
  #fi
}

##
## !! NOT USED !!
##
function ReloadBaseSchemas {
  ## Create temporary "PGPASSFILE" for 'psql' automatic password usage.
  ## See: http://www.postgresql.org/docs/current/static/libpq-pgpass.html
  export PGPASSFILE="/tmp/portal-admin.pgpass"
  echo "$DB_HOST:$DB_PORT:$DB_NAME:$DB_USER:$DB_PASS" > "$PGPASSFILE"
  ## If we don't set mode 600, 'psql' will ignore our password file.
  chmod 600 $PGPASSFILE

  for sqlScript in $DB_SCRIPTS; do
    echo "Importing script '$sqlScript'.."; echo
    psql -U $DB_USER -h $DB_HOST $DB_NAME < $sqlScript
    if [ "$?" -ne 0 ]; then
      echo "Error: DB import of script '$sqlScript' failed!"; echo
      Exit 1
    fi
  done
}

##
## !! NOT USED !!
##
function ReloadMagma {
  MAGMA_BUILD="$REPO_SRC/scripts/magma-build.sh"
  if [ ! -x $MAGMA_BUILD ]; then
    echo "Error: Magma build script not found or not executable!"
    echo "Skipping Magma build."
  else
    if [ -z "$MAGMA_FLAGS" ]; then
      echo "Error: Must supply MAGMA_FLAGS in profile '$PROFILE_DIR/$userProfile'"
      echo "Skipping Magma build."
    else
      cwd=`pwd`
      cd `dirname $MAGMA_BUILD`
      ./build.sh $MAGMA_FLAGS
      if [ $? -ne 0 ]; then
        echo "Error: Magma build exited with non-zero status."
        echo "Please check logs."
      fi
      cd $cwd
    fi
  fi
}

##
## !! NOT USED !!
##
function ReloadDatabase {
  ## Note: Currently ignoring target. Reloads DB in given DB settings.
  target="$1"

  CheckDatabaseCredentials
  ReloadBaseSchemas
  ReloadMagma
  echo; echo "Database reloaded."
}


###  MAIN  ###

## Perform some sanity checks first..

if [ -f $lockfile ]; then
  echo "WARNING: This script may already be in use! If safe to do so,
        remove the lock file '$lockfile' and re-run this script."
else
  touch $lockfile
fi

if [ "$#" -lt 4 ]; then
  Usage
fi
if [ "$1" != "-c" ]; then
  Usage
fi
if [ "${userProfile}X" = "X" ]; then
  echo "Error: Server profile settings file must be specified!"; echo
  Usage
fi
if [ ! -f "$PROFILE_DIR/$userProfile" ]; then
  echo "Error: Server profile '$PROFILE_DIR/$userProfile' not found!"; echo
  Usage
fi
if [ "${userInstance}X" = "X" ]; then
  echo "Error: Target pylons instance (config) must be specified!"; echo
  Usage
fi
if [ "${userCmd}X" = "X" ]; then
  echo "Error: Command must be specified!"; echo
  Usage
fi

## Load server profile. We do it here because the next test requires variables
## defined in the target profile.
source "$PROFILE_DIR/$userProfile"

if [ `IsValidTarget "$userInstance"` = "false" ]; then
  echo "Error: Pylons/paster target '$userInstance' is unknown!"
  Usage
fi


## Process user command

case $userCmd in
  start   ) StartPylons "$userInstance";;
  stop    ) StopPylons "$userInstance";;
  restart ) RestartPylons "$userInstance";;
  status  ) StatusPylons "$userInstance";;
  deploy  ) DeployPylons "$userInstance";;
#  dbreload) ReloadDatabase "$userInstance";;
  *       ) Usage;;
esac

Exit
