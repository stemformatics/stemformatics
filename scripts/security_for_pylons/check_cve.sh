#!/bin/bash

# Assuming that you can download via wget
# Assuming that you can access all the modules by pip freeze and that virtualenv is in /var/www/pylons/virtualenv


dir="/tmp/security_for_pylons/"
python_modules="/tmp/security_for_pylons/python_modules.txt"
all_cve="/tmp/security_for_pylons/allitems.txt"
mkdir $dir

ignore="requests six poster"

source /var/www/pylons/virtualenv/bin/activate
pip freeze > $python_modules

if [ -s "$all_cve" ]
then
    echo "continue"
else
    echo $dir
    cd $dir
    wget "http://cve.mitre.org/data/downloads/allitems.txt.gz"
    gzip -d allitems.txt.gz
fi

cat $python_modules | while read line
do
    echo "-----$line-----"
    module=`echo $line|cut -f1 -d'='`
    module=`echo $module|cut -f1 -d'.'`

    if [[ "$ignore" == *"$module"* ]]
    then
        echo "ignoring $module"
    else
        grep -wsHn "$module" "$all_cve"
        echo "-----end $line-----"
    fi
done
